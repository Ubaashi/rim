package users.main_office.main_controller;

import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import meta_data.AppData;
import meta_data.AppFunction;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class MainOfficeDashboard implements Initializable {
    @FXML
    private StackPane mainStackePane;

    @FXML
    private BorderPane borderPane;

    @FXML
    private ImageView ivClose;

    @FXML
    private ImageView ivMinimize;

    @FXML
    private Label lbUserId;

    @FXML
    private HBox btnDashboard;

    @FXML
    private ImageView ivDashboard;

    @FXML
    private Label lbDashboard;

    @FXML
    private HBox btnOrders;

    @FXML
    private ImageView ivOrders;

    @FXML
    private Label lbOrders;

    @FXML
    private HBox btnCustomers;

    @FXML
    private ImageView ivCustomers;

    @FXML
    private Label lbCustomers;

    @FXML
    private HBox btnProductionHouse;

    @FXML
    private ImageView ivProduction;

    @FXML
    private Label lbProductionHouse;

    @FXML
    private HBox btnTools;

    @FXML
    private ImageView ivTools;

    @FXML
    private Label lbTools;

    @FXML
    private HBox btnMaterails;

    @FXML
    private ImageView ivMaterails;

    @FXML
    private Label lbMaterals;

    @FXML
    private HBox btnWorkers;

    @FXML
    private ImageView ivWorkers;
    @FXML
    private ImageView ivNotification;

    @FXML
    private Label lbWorkers;

    @FXML
    private Circle cirNotifi;
    @FXML
    private AnchorPane topBar;
    @FXML
    private AnchorPane notificationPane;

    @FXML
    private Label lbNotificationCounter;

    @FXML
    private Label lbNewCountNotification;

    @FXML
    private JFXListView<?> lvNotification;

    private HashMap<ImageView, String[]> selctedIconsMap;
    private ImageView selectedImage;
    private boolean isMaximize=true;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        AppData.activeStackPane = mainStackePane;

        notificationPane.setVisible(false);

        lbUserId.setText("User Id : "+AppData.activeUser.getUserId());
        //Default Selected Dashboard
        setIcon();
        setSelectedBluePane(btnDashboard, lbDashboard);
        setCenter("/users/main_office/main_fxml/main_office_center_dashboard.fxml");


        ivNotification.setOnMouseClicked(event -> {
               notificationPane.setVisible(!notificationPane.isVisible());
        });


        mainStackePane.setOnMouseClicked(event -> {
             
        });


        btnDashboard.setOnMouseClicked(event -> {
            setSelectedBluePane(btnDashboard, lbDashboard);
            setSeletedIcon(ivDashboard);

            setCenter("/users/main_office/main_fxml/main_office_center_dashboard.fxml");

        });

        btnOrders.setOnMouseClicked(event -> {
            setSelectedBluePane(btnOrders, lbOrders);
            setSeletedIcon(ivOrders);
            setCenter("/users/main_office/main_fxml/orders.fxml");

        });
        btnCustomers.setOnMouseClicked(event -> {
            setSelectedBluePane(btnCustomers, lbCustomers);
            setSeletedIcon(ivCustomers);
            setCenter("/users/main_office/main_fxml/customers.fxml");
        });
        btnProductionHouse.setOnMouseClicked(event -> {
            setSelectedBluePane(btnProductionHouse, lbProductionHouse);
            setSeletedIcon(ivProduction);
            setCenter("/users/main_office/main_fxml/production_house.fxml");

        });
        btnTools.setOnMouseClicked(event -> {
            setSelectedBluePane(btnTools, lbTools);
            setSeletedIcon(ivTools);
            setCenter("/users/main_office/main_fxml/tools.fxml");

        });
        btnMaterails.setOnMouseClicked(event -> {
            setSelectedBluePane(btnMaterails, lbMaterals);
            setSeletedIcon(ivMaterails);
            setCenter("/users/main_office/main_fxml/materails.fxml");

        });

        btnWorkers.setOnMouseClicked(event -> {
            setSelectedBluePane(btnWorkers, lbWorkers);
            setSeletedIcon(ivWorkers);

            setCenter("/users/main_office/main_fxml/workers.fxml");

        });


        ivMinimize.setOnMouseClicked(event -> {
            AppFunction.windowMinimize(ivMinimize);
        });


        ivClose.setOnMouseClicked(event -> {
            System.exit(0);
        });


        //Rezising Stage
        topBar.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                if (isMaximize) {
                    AppData.mainStage.setMaximized(false);
                    AppData.mainStage.setFullScreen(false);
                    isMaximize = false;
                } else {
                    AppData.mainStage.setFullScreen(true);
                    AppData.mainStage.setMaximized(true);
                    isMaximize = true;
                }
            }
        });
    }

    private void setIcon() {
        selctedIconsMap = new HashMap<>();
        selctedIconsMap.put(ivDashboard, new String[]{"/icons/dashboard.png", "/icons/dashboard_selected.png"});
        selctedIconsMap.put(ivOrders, new String[]{"/icons/ic_order.png", "/icons/ic_orders_selected.png"});
        selctedIconsMap.put(ivCustomers, new String[]{"/icons/ic_Customer.png", "/icons/ic_customer_selected.png"});
        selctedIconsMap.put(ivProduction, new String[]{"/icons/ic_production_house.png", "/icons/ic_production_house_selected.png"});
        selctedIconsMap.put(ivTools, new String[]{"/icons/ic_tools.png", "/icons/ic_tools_selected.png"});
        selctedIconsMap.put(ivWorkers, new String[]{"/icons/ic_workers.png", "/icons/ic_worker_selected.png"});
        selctedIconsMap.put(ivMaterails, new String[]{"/icons/ic_machine.png", "/icons/ic_machine_selected.png"});

        ivDashboard.setImage(new Image("/icons/dashboard_selected.png"));
        selectedImage = ivDashboard;
        setUnSelectedIcon(ivOrders, 0);
        setUnSelectedIcon(ivCustomers, 0);
        setUnSelectedIcon(ivProduction, 0);
        setUnSelectedIcon(ivTools, 0);
        setUnSelectedIcon(ivMaterails, 0);
        setUnSelectedIcon(ivWorkers, 0);


    }

    private void setUnSelectedIcon(ImageView iv, int selectOption) {
        iv.setImage(new Image(selctedIconsMap.get(iv)[selectOption]));
    }

    private void setSelectedBluePane(HBox selectedPane, Label selectedLable) {
        HBox[] panes = new HBox[]{btnDashboard, btnOrders, btnCustomers, btnProductionHouse, btnTools, btnMaterails, btnWorkers};
        Label[] lables = new Label[]{lbDashboard, lbOrders, lbCustomers, lbProductionHouse, lbTools, lbMaterals, lbWorkers};

        for (HBox pane : panes) {
            pane.setStyle(" -fx-background-color: #65C7F7;");

        }
        for (Label label : lables) {
            label.setStyle("-fx-text-fill: #ffffff;" +
                    " -fx-font-weight: regular");
        }

        selectedPane.setStyle(" -fx-background-color: #1C1C1C");
        selectedLable.setStyle("-fx-text-fill: #65C7F7;" +
                " -fx-font-weight: bold");

    }

    private void setSeletedIcon(ImageView newSelection) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                selectedImage.setImage(new Image(selctedIconsMap.get(selectedImage)[0]));
                newSelection.setImage(new Image(selctedIconsMap.get(newSelection)[1]));
                selectedImage = newSelection;
            }
        }).start();

    }

    public void setCenter(String path) {
        try {
            Parent node = FXMLLoader.load(getClass().getResource(path));
            borderPane.setCenter(node);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
