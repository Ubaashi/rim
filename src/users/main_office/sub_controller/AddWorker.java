package users.main_office.sub_controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import meta_data.AppData;
import meta_data.AppFunction;
import meta_data.AppNotification;
import meta_data.MyQuery;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AddWorker implements Initializable {


    @FXML
    private ImageView ivClose;

    @FXML
    private JFXTextField tfWorkerId;

    @FXML
    private JFXTextField tfWorkerName;

    @FXML
    private JFXTextField tfEmail;

    @FXML
    private JFXTextField tfStreet;

    @FXML
    private JFXTextField tfPostalCode;

    @FXML
    private JFXTextField tfCity;

    @FXML
    private JFXCheckBox chkMainWorker;

    @FXML
    private JFXTextField tfUserName;

    @FXML
    private JFXTextField tfPassword;

    @FXML
    private Button btnAdd;

    private ArrayList<JFXTextField> textFields;

    private Connection connection;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        connection = AppData.connection;

        tfUserName.setEditable(false);
        tfPassword.setEditable(false);

        textFields = new ArrayList<>();
        textFields.add(tfWorkerId);
        textFields.add(tfWorkerName);
        textFields.add(tfEmail);
        textFields.add(tfStreet);
        textFields.add(tfPostalCode);
        textFields.add(tfCity);

        chkMainWorker.setOnMouseClicked(event -> {
            if (chkMainWorker.isSelected()) {

                tfUserName.setEditable(true);
                tfPassword.setEditable(true);
                textFields.add(tfUserName);
                textFields.add(tfPassword);

            } else {

                tfUserName.setEditable(false);
                tfPassword.setEditable(false);
                textFields.remove(tfUserName);
                textFields.remove(tfPassword);
            }
        });

        btnAdd.setOnAction(event -> {

            boolean isValidate = new AppFunction().validateTextFields(textFields);
            PreparedStatement statement;
            int lastInsertedUserId = -1;    //--- if worker is main user then this id wil be used in worker table as a foreign key of user table
            if (isValidate) {
                try {

                    if (chkMainWorker.isSelected()) {
                        //----------- First insert data to user table
                        String insertUserQuery = "INSERT into users(username,password,name,type,user_code) value (?,?,?,?,?)";
                        statement = connection.prepareStatement(insertUserQuery);
                        statement.setString(1, tfUserName.getText());
                        statement.setString(2, tfPassword.getText());
                        statement.setString(3, tfWorkerName.getText());
                        statement.setString(4, AppData.UserRole.MAIN_WORKER.name());
                        statement.setString(5, tfWorkerId.getText());

                        if (statement.executeUpdate() > 0) {
                            lastInsertedUserId = MyQuery.getLastInsertedId("users", "user_id");
                        } else {
                            AppNotification.getSuccessNotification("Something went wrong, Please try again");
                            return;
                        }
                    }

                    //----------- insert date to worker table
                    String workerQuery = "INSERT  into workers(name,email,is_main_worker,street,postal_code,city,user_id,code) values (?,?,?,?,?,?,?,?)";

                    statement = connection.prepareStatement(workerQuery);
                    statement.setString(1, tfWorkerName.getText());
                    statement.setString(2, tfEmail.getText());
                    statement.setBoolean(3, chkMainWorker.isSelected());
                    statement.setString(4, tfStreet.getText());
                    statement.setString(5, tfPostalCode.getText());
                    statement.setString(6, tfCity.getText());
                    statement.setInt(7, lastInsertedUserId);
                    statement.setString(8, tfWorkerId.getText());

                    if (statement.executeUpdate() > 0) {
                        AppNotification.getSuccessNotification("Worker account create successful");
                    } else {
                        AppNotification.getErrorNotification("Something went wrong, Please try again");
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

        });


        ivClose.setOnMouseClicked(event -> {
            AppData.dialog.close();
        });
    }
}
