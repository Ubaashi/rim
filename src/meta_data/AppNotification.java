package meta_data;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class AppNotification {

    public static Notifications getNotification(String title, String text, Image image) {

        Notifications notifications = Notifications.create()
                .title(title)
                .text(text)
                .graphic(null)
                .position(Pos.TOP_RIGHT)
                .hideAfter(Duration.seconds(3))
                .darkStyle();

        return notifications;
    }

    public static void getErrorNotification(String text) {

        Notifications.create()
                .title("Error")
                .text(text)
                .graphic(null)
                .position(Pos.TOP_RIGHT)
                .hideAfter(Duration.seconds(3))
                .darkStyle().showError();


    }

    public static void getSuccessNotification(String text) {

        Notifications.create()
                .title("Success")
                .text(text)
                .graphic(null)
                .position(Pos.TOP_RIGHT)
                .hideAfter(Duration.seconds(3))
                .darkStyle().showInformation();


    }
}
