package meta_data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MyQuery {



    public static void delete(String table, String where, String perameter) {
        PreparedStatement statement = null;
        try {
            statement = AppData.connection.prepareStatement("" +
                    "DELETE FROM " + table + " WHERE " + where + "=?");

            statement.setString(1, perameter);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet query(String query) {
        try {
            PreparedStatement statement = AppData.connection.prepareStatement(query);
            return statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int getLastInsertedId(String table, String id) {
        try {
            PreparedStatement statement = AppData.connection.prepareStatement(
                    "SELECT * FROM " + table + " ORDER by " + id + " DESC limit 1"
            );
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    private static int getIdFromTExt(String id, String table, String where, String perameter) {
        try {
            PreparedStatement statement = AppData.connection.prepareStatement(
                    "SELECT " + id + " FROM " + table + " WHERE " + where + " =" + perameter
            );
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static boolean isRecordAlreadyExists(String tableName, String fieldName, String parameter) {

        try {
            PreparedStatement statement = AppData.connection.prepareStatement("" +
                    "SELECT " + fieldName + " FROM " + tableName + " WHERE " + fieldName + "=?");
            statement.setString(1, parameter);
            ResultSet re = statement.executeQuery();
            if (re.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean isDataExists(String tableName) {

        try {
            PreparedStatement statement = AppData.connection.prepareStatement("" +
                    "SELECT * FROM " + tableName );
            ResultSet re = statement.executeQuery();
            if (re.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

