package users.main_office.list_cell.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import users.main_office.models.WorkerModel;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class Worker_cell  extends ListCell<WorkerModel> {


    private FXMLLoader fxmlLoader;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label lbId;

    @FXML
    private Label lbName;

    @FXML
    private Label lbAddress;

    @FXML
    private Label lbContact;

    @FXML
    private Label lbEmail;

    @Override
    protected void updateItem(WorkerModel model, boolean empty) {
        super.updateItem(model, empty);


        if (model == null || empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (fxmlLoader == null) {
                fxmlLoader = new FXMLLoader(getClass().getResource("/users/main_office/list_cell/fxml/worker_cell.fxml"));
                fxmlLoader.setController(this);
                try {
                    fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            lbId.setText(model.getWorkerCode());
            lbName.setText(model.getWorkerName());
            lbAddress.setText(model.getStreet()+","+model.getCity()+","+model.getPostalCode());
            lbContact.setText(model.getWorkerCode());
            lbEmail.setText(model.getWorkerEmail());

            setText(null);
            setGraphic(pane);
        }
    }


}
