package meta_data;

import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;

public class AppFunction {


    public static boolean isEmpty(TextField textField) {
        if (textField.getText().trim().isEmpty()) {
            AppNotification.getSuccessNotification(textField.getPromptText() + " must be required");
            textField.requestFocus();
            return false;
        } else {
            return true;
        }


    }

    public static boolean isEmpty(JFXTextField textField) {
        if (textField.getText().trim().isEmpty()) {
            AppNotification.getSuccessNotification(textField.getPromptText() + " must be required");
            textField.requestFocus();
            return false;
        } else {
            return true;
        }


    }



    public boolean validateComboboxs(ComboBox[] list) {
        boolean validationStatus = true;

        for (ComboBox combobox :
                list) {
            if (combobox.getSelectionModel().getSelectedIndex() == -1) {
                combobox.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;-fx-text-fill: red; -fx-prompt-text-fill: red");
                combobox.setPromptText(combobox.getPromptText() + " required");


                combobox.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
                    if (newPropertyValue) {
                        combobox.setStyle("-fx-border-color: grey ; -fx-border-width: 2px ;-fx-text-fill: black;-fx-prompt-text-fill: grey");
                        combobox.setPromptText(combobox.getPromptText().replace(" required", ""));
                    } else {
                        System.out.println("Combobox out focus");
                    }

                });
                validationStatus = false;
            }
        }


        return validationStatus;
    }

    public boolean validateTextFields(ArrayList<JFXTextField> list) {
        boolean validationStatus = true;

        for (TextField textField :
                list) {
            if (textField.getText().isEmpty()) {
                textField.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;-fx-text-fill: red; -fx-prompt-text-fill: red");
                textField.setPromptText(textField.getPromptText() + " required");

                textField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
                    if (newPropertyValue) {
                        textField.setPromptText(textField.getPromptText().replace(" required", ""));
                        textField.setStyle("-fx-border-color: grey ; -fx-border-width: 2px ;-fx-text-fill: white;-fx-prompt-text-fill: #3399ff");
                    } else {
                        System.out.println("Textfield out focus");
                    }


                });
                validationStatus = false;
            }
        }


        return validationStatus;
    }


    public boolean validatePassTextFields(ArrayList<JFXPasswordField> list) {
        boolean validationStatus = true;

        for (JFXPasswordField passwordField :
                list) {
            if (passwordField.getText().isEmpty()) {
                passwordField.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;-fx-text-fill: red; -fx-prompt-text-fill: red");
                passwordField.setPromptText(passwordField.getPromptText() + " required");

                passwordField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
                    if (newPropertyValue) {
                        passwordField.setPromptText(passwordField.getPromptText().replace(" required", ""));
                        passwordField.setStyle("-fx-border-color: grey ; -fx-border-width: 2px ;-fx-text-fill: white;-fx-prompt-text-fill: #3399ff");
                    } else {
                        System.out.println("Textfield out focus");
                    }


                });
                validationStatus = false;
            }
        }


        return validationStatus;
    }

    public boolean validateTextFields(TextArea[] list) {
        boolean validationStatus = true;

        for (TextArea textField :
                list) {
            if (textField.getText().isEmpty()) {
                textField.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;-fx-text-fill: red; -fx-prompt-text-fill: red");
                textField.setPromptText(textField.getPromptText() + " required");


                textField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
                    if (newPropertyValue) {
                        textField.setStyle("-fx-border-color: grey ; -fx-border-width: 2px ;-fx-text-fill: black;-fx-prompt-text-fill: grey");
                        textField.setPromptText(textField.getPromptText().replace(" required", ""));
                    } else {
                        System.out.println("Textfield out focus");
                    }


                });
                validationStatus = false;
            }
        }


        return validationStatus;
    }


    public void setNumberFormatter(TextField[] textFields) {

        for (TextField textField : textFields
                ) {

            textField.textProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue.matches("[0-9]*")) {
                    textField.setText(oldValue);
                }
            });
        }

    }


    public void showDialog(String path) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource(path));
            new AppDialog().showDialog(root, JFXDialog.DialogTransition.RIGHT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void showDialog(String path, boolean overlay) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource(path));
            new AppDialog().showDialog(root, JFXDialog.DialogTransition.RIGHT, overlay);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void setImageToCircle(Image image, Circle cr_image, String defaultImagePath) {

        // If Image Null then set default path image
        if (image == null) {
            image = new Image(defaultImagePath);
        }
        ImagePattern pattern = new ImagePattern(image);
        cr_image.setFill(pattern);
    }

    public static void setImageToRectangle(Image image, Rectangle rectangle, String defaultImagePath) {

        // If Image Null then set default path image
        if (image == null) {
            image = new Image(defaultImagePath);
        }
        ImagePattern pattern = new ImagePattern(image);
        rectangle.setFill(pattern);
    }

    public static void windowMinimize(Node btnMinimize) {
        Stage stage = (Stage) btnMinimize.getScene().getWindow();
        stage.setIconified(true);
    }


    public void setDatePickerFormatter(DatePicker datePicker) {

        datePicker.setConverter(new StringConverter<LocalDate>() {
            private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null)
                    return "";
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });
    }


    public static void bindTextField(TextField textField, String columnName, String tableName) {
        ObservableList<String> list = FXCollections.observableArrayList();
        try {

            PreparedStatement statement = AppData.connection.prepareStatement(
                    "SELECT DISTINCT(" + columnName + ") FROM " + tableName
            );
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(resultSet.getString(columnName));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TextFields.bindAutoCompletion(textField, list);

    }

    public static void bindTextField(TextField textField, String columnName, String tableName, String where) {
        ObservableList<String> list = FXCollections.observableArrayList();
        try {

            PreparedStatement statement = AppData.connection.prepareStatement(
                    "SELECT DISTINCT(" + columnName + ") FROM " + tableName + " WHERE " + where
            );
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(resultSet.getString(columnName));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TextFields.bindAutoCompletion(textField, list);

    }

    public static int insertNotification(String title, String description, String sendFrom, String sendTo, int setForeginID) {
        try {
            PreparedStatement statement = AppData.connection.prepareStatement("INSERT INTO notifications" +
                    "(title, " +
                    "description, " +
                    "notificationDateTime, " +
                    "sendedTo, " +
                    "sendedFrom, " +
                    "viewStatus, " +
                    "setForeginId) " +
                    "VALUES (?,?,?,?,?,?,?)");
            statement.setString(1, title);
            statement.setString(2, description);
            statement.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
            statement.setString(4, sendTo);
            statement.setString(5, sendFrom);
            statement.setInt(6, 0);
            statement.setInt(7, setForeginID);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void showReport(String path, Map<String, Object> map, boolean isPrint) {

        try {
            InputStream is = getClass().getResourceAsStream(path);
            JasperReport jasperReport = null;
            jasperReport = JasperCompileManager.compileReport(is);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, AppData.connection);


            if (isPrint) {
                JasperPrintManager.printReport(jasperPrint, false);
            } else {
                JasperViewer.viewReport(jasperPrint, false);
            }


        } catch (JRException e) {
            e.printStackTrace();
        }
    }


    public void playNotificationSound() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                URL re = getClass().getResource("/sound_files/notification_sound.wav");
                Media hit = new Media(re.toString());
                MediaPlayer mediaPlayer = new MediaPlayer(hit);
                mediaPlayer.play();
                System.out.println("play sound");

            }
        }).start();


    }

    public static void updateNotificationStatus(int notificationId, int status) {
        //Notification Status  0 for not view . 1 For Recieved 2 Fro Seen
        try {
            PreparedStatement statement = AppData.connection.prepareStatement(
                    "UPDATE notifications SET viewStatus=? WHERE notificationId=?"
            );

            statement.setInt(1, status);
            statement.setInt(2, notificationId);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getCalculatedTime(Timestamp notificationDateTime) {
        long timeStart = notificationDateTime.getTime();
        long timeStop = Timestamp.valueOf(LocalDateTime.now()).getTime();

        try {

            //in milliseconds
            long diff = timeStart - timeStop;

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if (diffDays != 0)
                return String.valueOf(-diffDays) + " Day";
            if (diffHours != 0)
                return String.valueOf(-diffHours) + " Hor";
            if (diffMinutes != 0)
                return String.valueOf(-diffMinutes) + " Min";
            if (diffSeconds != 0)
                return String.valueOf(-diffSeconds) + " Sec";

        } catch (Exception e) {
            e.printStackTrace();
        }
        return " ";
    }

    public static void updatePatientAccount(int patientId, double amount) {
        System.out.println("PatientId:" + patientId + "  Amount:" + amount);
        try {
            //Update Account Amount
            PreparedStatement updateStatement = AppData.connection.prepareStatement("" +
                    " UPDATE patient_account SET TotalAmount=TotalAmount+?  " +
                    " WHERE PatientId=?\n"
            );
            updateStatement.setDouble(1, amount);
            updateStatement.setInt(2, patientId);
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateEmployessAccount(int employeeId, double amount) {
        System.out.println("PatientId:" + employeeId + "  Amount:" + amount);
        try {
            //Update Account Amount
            PreparedStatement updateStatement = AppData.connection.prepareStatement("" +
                    " UPDATE employee_account SET TotalBalance=TotalBalance+?  " +
                    " WHERE employeeId=?\n"
            );
            updateStatement.setDouble(1, amount);
            updateStatement.setInt(2, employeeId);
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public static int getEmployeeAccountId(int employeeId) {

        String employeeAccountQuery = "SELECT employeeAccountId as id FROM employee_account WHERE employeeId=?";

        int id = 0;
        try {
            PreparedStatement stEmployeeAccount = AppData.connection.prepareStatement(employeeAccountQuery);
            stEmployeeAccount.setInt(1, employeeId);
            ResultSet resultSet = stEmployeeAccount.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static int getPatientAccountId(int patientId) {

        String patientAccountQuery = "SELECT * FROM patient_account WHERE PatientId=?";

        int patientAccountId = 0;
        try {
            PreparedStatement stPatientAccount = AppData.connection.prepareStatement(patientAccountQuery);
            stPatientAccount.setInt(1, patientId);
            ResultSet resultSet = stPatientAccount.executeQuery();
            if (resultSet.next()) {
                patientAccountId = resultSet.getInt("patientAccountId");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patientAccountId;
    }


    public static int insertPatientAccountIfNotExists(int patientId, double amount) {

        String patientAccountQuery = "SELECT * FROM patient_account WHERE PatientId=?";

        int patientAccountId = 0;
        try {
            PreparedStatement stPatientAccount = AppData.connection.prepareStatement(patientAccountQuery);
            stPatientAccount.setInt(1, patientId);
            ResultSet resultSet = stPatientAccount.executeQuery();
            if (resultSet.next()) {
                patientAccountId = resultSet.getInt("patientAccountId");
            } else {
                //Insert New Account
                PreparedStatement insertStatement = AppData.connection.prepareStatement("" +
                        "INSERT INTO patient_account (PatientId, TotalAmount) VALUES (?,?) \n"
                );
                insertStatement.setInt(1, patientId);
                insertStatement.setDouble(2, amount);
                insertStatement.execute();

                patientAccountId = MyQuery.getLastInsertedId("patient_account", "patientAccountId");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patientAccountId;
    }

    public static int insertEmplyeeAccountIfNotExists(int employeeId, double amount) {

        String patientAccountQuery = "SELECT * FROM employee_account WHERE employeeId=?";

        int patientAccountId = 0;
        try {
            PreparedStatement stPatientAccount = AppData.connection.prepareStatement(patientAccountQuery);
            stPatientAccount.setInt(1, employeeId);
            ResultSet resultSet = stPatientAccount.executeQuery();
            if (resultSet.next()) {
                patientAccountId = resultSet.getInt("employeeAccountId");
            } else {
                //Insert New Account
                PreparedStatement insertStatement = AppData.connection.prepareStatement("" +
                        "INSERT INTO employee_account (employeeId, TotalBalance) VALUES (?,?) \n"
                );
                insertStatement.setInt(1, employeeId);
                insertStatement.setDouble(2, amount);
                insertStatement.execute();

                patientAccountId = MyQuery.getLastInsertedId("employee_account", "employeeId");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patientAccountId;
    }



    public static void clearAllTextFields(TextField[] textFieldList) {

        for (TextField textField : textFieldList
                ) {
            textField.clear();
        }

    }


    public static int getTotal(String table) {
        int total = 0;
        try {
            PreparedStatement statement = AppData.connection.prepareStatement("" +
                    "SELECT COUNT(*) AS total FROM " + table);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                total = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;

    }

    public static int getTotalHospitalEarining() {
        int total = 0;
        try {
            PreparedStatement statement = AppData.connection.prepareStatement("" +
                    "SELECT sum(PaidAmount) AS total FROM patient_account_details");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                total = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;

    }

    public String formatDateTime(LocalDateTime localDateTime) {

        DateTimeFormatter myFormateObj = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a");
        return myFormateObj.format(localDateTime);
    }

    public static String formatDate(LocalDate localDateTime, String formate) {

        DateTimeFormatter myFormateObj = DateTimeFormatter.ofPattern(formate);
        return myFormateObj.format(localDateTime);
    }



    public static int getServicesTotalPatinetsCount() {
        int total = 0;
        try {
            PreparedStatement statement = AppData.connection.prepareStatement("SELECT count(DISTINCT(appointmentId)) as total FROM patientextraservces");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                total = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;

    }

    public static int getServicesTotalPatinetsTodaysCount() {
        int total = 0;
        try {
            PreparedStatement statement = AppData.connection.prepareStatement("SELECT count(DISTINCT(appointmentId)) as total\n" +
                    "FROM patientextraservces\n" +
                    "WHERE DATE(provideDateTime) = CURDATE() ");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                total = resultSet.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;

    }
}

