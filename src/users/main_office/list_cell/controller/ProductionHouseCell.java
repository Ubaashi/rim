package users.main_office.list_cell.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

// Colors codes :
// Yellow #F8EE95
// Green #84F688
// Red #F799A8

public class ProductionHouseCell extends ListCell {


    @FXML
    private Label lbCode;

    @FXML
    private Label lbDeliveryDate;

    @FXML
    private Label lbQuantity;

    @FXML
    private Label lbPrice;

    @FXML
    private Button btnOpen;

    @FXML
    private Button btnEdit;

//    private FXMLLoader fxmlLoader;
//
//    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd,yyyy");
//
//    @Override
//    protected void updateItem(PatientModel model, boolean empty) {
//        super.updateItem(model, empty);
//
//
//        if (model == null || empty) {
//            setText(null);
//            setGraphic(null);
//        } else {
//            if (fxmlLoader == null) {
//                fxmlLoader = new FXMLLoader(getClass().getResource("/listview_cells/fxml/patient_appointment.fxml"));
//                fxmlLoader.setController(this);
//                try {
//                    fxmlLoader.load();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//            lbDate.setText(formatter.format(model.getAppointmentDate()));
//            lbTime.setText(model.getAppointmentTime());
//
//            setText(null);
//            setGraphic(pane);
//        }
//    }


}
