package meta_data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class DbConnect {


    public Connection connection() {

        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

            String ipAddress = getIpAddressFromTextFile();
            AppData.serverIpAddress = ipAddress;

            //WMC user / Password
            connection = DriverManager.getConnection("jdbc:mysql://" + ipAddress + "/"+ AppData.dbName+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", AppData.dbUser, AppData.dbPassword);
//
//            connection = DriverManager.getConnection("jdbc:mysql://" + ipAddress + "/hms?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", null);


            connection.createStatement();

        } catch (InstantiationException | IllegalAccessException | SQLException | ClassNotFoundException e) {
//            AppNotification.getErrorNotification(e.getMessage()).showError();
            e.printStackTrace();
        }
        return connection;
    }

    private String getIpAddressFromTextFile() {
        FileReader reader = null;
        String ipaddress = null;
        try {
            reader = new FileReader("serverIpAddress.txt");

            Scanner s = new Scanner(reader);

            if (s.hasNextLine()) {
                ipaddress = s.next();
            } else {
                AppNotification.getErrorNotification("Ip Address not found");
            }

            reader.close();
            System.out.println("Ip Address:" + ipaddress);
            return ipaddress;
        } catch (FileNotFoundException e) {
            AppNotification.getErrorNotification("Ip Address not found");

        } catch (Exception e) {
            AppNotification.getErrorNotification("Ip Address not found");

        }
        return ipaddress;

    }


}
