package login;

import app_models.UserModel;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.sun.javafx.PlatformUtil;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import meta_data.AppData;
import meta_data.AppFunction;
import meta_data.DbConnect;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class Login implements Initializable {

    @FXML
    private JFXTextField tfUserName;

    @FXML
    private JFXPasswordField tfPassword;

    @FXML
    private Button btnLogin;
    @FXML
    private Label lbError;

    DbConnect dbConnect;
    Connection connection;

    private ArrayList<JFXTextField> textFields;
    private ArrayList<JFXPasswordField> pass_textFields;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dbConnect = new DbConnect();
        connection = dbConnect.connection();

        textFields = new ArrayList<>();
        pass_textFields = new ArrayList<>();

        textFields.add(tfUserName);
        pass_textFields.add(tfPassword);

        AppData.connection = connection;

        lbError.setVisible(false);

        tfPassword.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                btnLogin.fire();
            }
        });
        tfUserName.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                tfPassword.requestFocus();
            }
        });

        btnLogin.setOnAction(event -> {

            boolean isValidate = new AppFunction().validateTextFields(textFields);
            boolean isPassValidate = new AppFunction().validatePassTextFields(pass_textFields);

            if (isValidate && isPassValidate) {
                try {
                    PreparedStatement loginStmt = connection.prepareStatement("SELECT * FROM users WHERE username=? AND password=?");
                    loginStmt.setString(1, tfUserName.getText());
                    loginStmt.setString(2, tfPassword.getText());

                    ResultSet resultSet = loginStmt.executeQuery();
                    if (resultSet.next()) {

                        AppData.activeUser = new UserModel(
                                resultSet.getInt("user_id"),
                                resultSet.getString("username"),
                                resultSet.getString("password"),
                                resultSet.getString("name"),
                                resultSet.getString("type"),
                                resultSet.getString("user_code")
                        );

                        newStageOpen("/users/main_office/main_fxml/main_office_dashboard.fxml", true);

                    } else {
                        lbError.setText("Username or password is wrong");
                        lbError.setVisible(true);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void newStageOpen(String path, boolean isFullScreen) {
        Parent root = null;
        AppData.mainStage.close();
        try {
            root = FXMLLoader.load(getClass().getResource(path));
            Scene scene = new Scene(root);
            scene.setFill(Color.TRANSPARENT);

            AppData.mainStage.setScene(scene);

            if (PlatformUtil.isWindows()) {

                AppData.mainStage.setMaximized(isFullScreen);
                AppData.mainStage.setFullScreen(isFullScreen);
            } else {
                if (isFullScreen) {
                    Screen screen = Screen.getPrimary();
                    Rectangle2D bounds = screen.getVisualBounds();

                    AppData.mainStage.setX(bounds.getMinX());
                    AppData.mainStage.setY(bounds.getMinY());
                    AppData.mainStage.setWidth(bounds.getWidth());
                    AppData.mainStage.setHeight(bounds.getHeight());
                }
            }
            root.setOnMousePressed(event -> {
                AppData.xOffset = event.getSceneX();
                AppData.yOffset = event.getSceneY();
            });

            root.setOnMouseDragged(event -> {
                AppData.mainStage.setX(event.getScreenX() - AppData.xOffset);
                AppData.mainStage.setY(event.getScreenY() - AppData.yOffset);
            });
            AppData.mainStage.hide();

            AppData.mainStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
