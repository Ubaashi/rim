package users.main_office.sub_controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import meta_data.AppData;
import meta_data.AppFunction;
import meta_data.AppNotification;
import meta_data.MyQuery;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AddNewAccount implements Initializable {

    @FXML
    private ImageView ivClose;

    @FXML
    private JFXTextField tfUserId;

    @FXML
    private JFXTextField tfName;

    @FXML
    private JFXTextField tfEmail;

    @FXML
    private JFXTextField tfStreet;

    @FXML
    private JFXTextField tfPostalCode;

    @FXML
    private JFXTextField tfCity;

    @FXML
    private JFXTextField tfUserName;

    @FXML
    private JFXTextField tfPassword;

    @FXML
    private Button btnAdd;

    @FXML
    private Label lbTitle;

    private Connection connection;
    private ArrayList<JFXTextField> textFields;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        connection = AppData.connection;

        setRoleOfNewAccountCraeting(); //New Account da cha dapara create kegi

        textFields = new ArrayList<>();
        textFields.add(tfUserId);
        textFields.add(tfName);
        textFields.add(tfEmail);
        textFields.add(tfStreet);
        textFields.add(tfPostalCode);
        textFields.add(tfCity);
        textFields.add(tfUserName);
        textFields.add(tfPassword);


//        btnAdd.setOnAction(event -> {
//
//            boolean isValidate = new AppFunction().validateTextFields(textFields);
//            PreparedStatement statement;
//            int lastInsertedUserId = -1;    //--- if worker is main user then this id wil be used in worker table as a foreign key of user table
//            if (isValidate) {
//                try {
//
//                    if (chkMainWorker.isSelected()) {
//                        //----------- First insert data to user table
//                        String insertUserQuery = "INSERT into users(username,password,name,type,user_code) value (?,?,?,?,?)";
//                        statement = connection.prepareStatement(insertUserQuery);
//                        statement.setString(1, tfUserName.getText());
//                        statement.setString(2, tfPassword.getText());
//                        statement.setString(3, tfWorkerName.getText());
//                        statement.setString(4, AppData.UserRole.MAIN_WORKER.name());
//                        statement.setString(5, tfWorkerId.getText());
//
//                        if (statement.executeUpdate() > 0) {
//                            lastInsertedUserId = MyQuery.getLastInsertedId("users", "user_id");
//                        } else {
//                            AppNotification.getSuccessNotification("Something went wrong, Please try again");
//                            return;
//                        }
//                    }
//
//                    //----------- insert date to worker table
//                    String workerQuery = "INSERT  into workers(name,email,is_main_worker,street,postal_code,city,user_id,code) values (?,?,?,?,?,?,?,?)";
//
//                    statement = connection.prepareStatement(workerQuery);
//                    statement.setString(1, tfWorkerName.getText());
//                    statement.setString(2, tfEmail.getText());
//                    statement.setBoolean(3, chkMainWorker.isSelected());
//                    statement.setString(4, tfStreet.getText());
//                    statement.setString(5, tfPostalCode.getText());
//                    statement.setString(6, tfCity.getText());
//                    statement.setInt(7, lastInsertedUserId);
//                    statement.setString(8, tfWorkerId.getText());
//
//                    if (statement.executeUpdate() > 0) {
//                        AppNotification.getSuccessNotification("Worker account create successful");
//                    } else {
//                        AppNotification.getErrorNotification("Something went wrong, Please try again");
//                    }
//                } catch (SQLException throwables) {
//                    throwables.printStackTrace();
//                }
//            }
//
//        });


        ivClose.setOnMouseClicked(event -> {
            AppData.dialog.close();
        });
    }

    private void setRoleOfNewAccountCraeting() {
        if (AppData.newUserAddRole.equals(AppData.UserRole.PRODUCTION_HOUSE.name())){
            lbTitle.setText("Production House User Information");
        }  if (AppData.newUserAddRole.equals(AppData.UserRole.TOOLS.name())){
            lbTitle.setText("Tools User Information");
        }  if (AppData.newUserAddRole.equals(AppData.UserRole.MATERIAL.name())){
            lbTitle.setText("Material User Information");
        }
    }
}
