package users.main_office.list_cell.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

public class NotificationCell extends ListCell {

    @FXML
    private AnchorPane pane;

    @FXML
    private Label lbTitle;

    @FXML
    private Label lbDescription;

    @FXML
    private Circle circule;

    @FXML
    private Label lbTime;

    @FXML
    private Label lbSendFrom;

//    private FXMLLoader fxmlLoader;
//
//    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd,yyyy");
//
//    @Override
//    protected void updateItem(PatientModel model, boolean empty) {
//        super.updateItem(model, empty);
//
//
//        if (model == null || empty) {
//            setText(null);
//            setGraphic(null);
//        } else {
//            if (fxmlLoader == null) {
//                fxmlLoader = new FXMLLoader(getClass().getResource("/listview_cells/fxml/patient_appointment.fxml"));
//                fxmlLoader.setController(this);
//                try {
//                    fxmlLoader.load();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//            lbDate.setText(formatter.format(model.getAppointmentDate()));
//            lbTime.setText(model.getAppointmentTime());
//
//            setText(null);
//            setGraphic(pane);
//        }
//    }


}
