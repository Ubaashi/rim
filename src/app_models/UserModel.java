package app_models;

import org.hsqldb.User;

public class UserModel {

    int userId;
    String username;
    String password;
    String name;
    String usrType;
    String userCode;

    public UserModel(int userId, String username, String password, String name, String usrType, String userCode) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.name = name;
        this.usrType = usrType;
        this.userCode = userCode;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getUsrType() {
        return usrType;
    }

    public String getUserCode() {
        return userCode;
    }
}
