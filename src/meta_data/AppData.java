package meta_data;

import app_models.UserModel;
import com.jfoenix.controls.JFXDialog;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.sql.Connection;

public class AppData {


    public static Stage mainStage;
    public static JFXDialog dialog;
    public static StackPane activeStackPane;
    public static Connection connection;
    public static String serverIpAddress;
    public static String dbName="rim";
    public static String dbUser="root";
    public static String dbPassword=null;
    public static double xOffset;
    public static double yOffset;
    public static UserModel activeUser;
    public static String newUserAddRole;


    public enum UserRole {
        MAIN_OFFICE,
        PRODUCTION_HOUSE,
        MATERIAL,
        TOOLS,
        MAIN_WORKER,
        MAIN_RECEPTION,
    }


}
