package meta_data;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.Parent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class AppDialog {

    public AppDialog() {
    }

    public void showDialog(Parent parent, JFXDialog.DialogTransition transition, boolean overlay) {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setBody(parent);
        layout.getStyleClass().remove("jfx-dialog-layout");

        JFXDialog dialog = new JFXDialog(AppData.activeStackPane, layout, transition, overlay);
        AppData.dialog = dialog;
        System.out.println(dialog.getPrefWidth() + " " + dialog.getPrefHeight());
        dialog.show();

    }

    public void showDialog(Parent parent, JFXDialog.DialogTransition transition) {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setBody(parent);
        layout.getStyleClass().remove("jfx-dialog-layout");

        JFXDialog dialog = new JFXDialog(AppData.activeStackPane, layout, transition);
        dialog.setStyle("-fx-fill-color: red");
        AppData.dialog = dialog;
        System.out.println(dialog.getPrefWidth() + " " + dialog.getPrefHeight());
        dialog.show();

    }

    public void showRegistration(StackPane stackPane, Parent parent, JFXDialog.DialogTransition transition) {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setBody(parent);
        layout.getStyleClass().remove("jfx-dialog-layout");

        JFXDialog dialog = new JFXDialog(stackPane, layout, transition);
        AppData.dialog = dialog;
        System.out.println(dialog.getPrefWidth() + " " + dialog.getPrefHeight());
        dialog.show();

    }

    public void showConfirmationDialog(Parent root, JFXDialog.DialogTransition transition) {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setBody(root);
        layout.getStyleClass().remove("jfx-dialog-layout");

        JFXDialog dialog = new JFXDialog(AppData.activeStackPane, layout, transition);
        AppData.dialog = dialog;
        System.out.println(dialog.getPrefWidth() + " " + dialog.getPrefHeight());
        dialog.show();
    }
}
