package users.main_office.main_controller;

import com.jfoenix.controls.JFXComboBox;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import meta_data.AppFunction;

import java.net.URL;
import java.util.ResourceBundle;

public class Orders implements Initializable {

    @FXML
    private Button btnCreateNewOrder;

    @FXML
    private JFXComboBox<?> cmbFilter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btnCreateNewOrder.setOnAction(event -> {
            new AppFunction().showDialog("/users/main_office/sub_fxml/create_new_order.fxml");
        });

    }
}
