package users.main_office.sub_controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import meta_data.AppData;

import java.net.URL;
import java.util.ResourceBundle;

public class CreateNewOrder implements Initializable {


    @FXML
    private ImageView ivClose;

    @FXML
    private JFXTextField tfWorkerId;

    @FXML
    private JFXTextField tfWorkerName;

    @FXML
    private JFXTextField tfEmail;

    @FXML
    private JFXTextField tfStreet;

    @FXML
    private JFXTextField tfPostalCode;

    @FXML
    private JFXTextField tfCity;

    @FXML
    private JFXCheckBox chkMainWorker;

    @FXML
    private JFXTextField tfUserName;

    @FXML
    private JFXTextField tfPassword;

    @FXML
    private Button btnAdd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        ivClose.setOnMouseClicked(event -> {
            AppData.dialog.close();
        });
    }


}
