package users.main_office.main_controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import meta_data.AppController;
import meta_data.AppData;
import meta_data.AppFunction;
import users.main_office.list_cell.controller.Worker_cell;
import users.main_office.models.WorkerModel;

import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class Workers implements Initializable {

    @FXML
    private Button btnAddWorker;

    @FXML
    private JFXComboBox<?> cmbFilter;

    @FXML
    JFXListView<WorkerModel> lvWorker;

    private Connection connection;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        connection = AppData.connection;
        setDataToListView();
        btnAddWorker.setOnAction(event -> {
            new AppFunction().showDialog("/users/main_office/sub_fxml/add_worker.fxml");
        });
    }

    private void setDataToListView() {

//----------- Get patient list from database --------------//
        try {
            PreparedStatement patientStmt = connection.prepareStatement("SELECT * from workers ");
            ResultSet resultSet = patientStmt.executeQuery();

            ObservableList<WorkerModel> workerList = FXCollections.observableArrayList();

            while (resultSet.next()) {
                workerList.add(new WorkerModel(
                        resultSet.getInt("worker_id"),
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getBoolean("is_main_worker"),
                        resultSet.getString("street"),
                        resultSet.getInt("postal_code"),
                        resultSet.getInt("user_id"),
                        resultSet.getString("code"),
                        resultSet.getString("city")
                ));
            }
            lvWorker.setItems(workerList);

            lvWorker.setCellFactory(param -> new Worker_cell());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
