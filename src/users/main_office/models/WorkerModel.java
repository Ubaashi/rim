package users.main_office.models;

public class WorkerModel {
    int workerId;
    String workerName;
    String workerEmail;
    String workerContact;
    String city;
    boolean isMainWorker;
    String street;
    int postalCode;
    int userId;
    String workerCode;

    public WorkerModel(int workerId, String workerName, String workerEmail, boolean isMainWorker, String street, int postalCode, int userId, String workerCode,String city) {
        this.workerId = workerId;
        this.workerName = workerName;
        this.workerEmail = workerEmail;
        this.isMainWorker = isMainWorker;
        this.street = street;
        this.postalCode = postalCode;
        this.userId = userId;
        this.workerCode = workerCode;
        this.city=city;
    }

    public String getCity() {
        return city;
    }

    public int getWorkerId() {
        return workerId;
    }

    public String getWorkerName() {
        return workerName;
    }

    public String getWorkerEmail() {
        return workerEmail;
    }

    public boolean isMainWorker() {
        return isMainWorker;
    }

    public String getStreet() {
        return street;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public int getUserId() {
        return userId;
    }

    public String getWorkerCode() {
        return workerCode;
    }
}
