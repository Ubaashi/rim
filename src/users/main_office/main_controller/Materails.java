package users.main_office.main_controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import meta_data.AppData;
import meta_data.AppFunction;

import java.net.URL;
import java.util.ResourceBundle;

public class Materails implements Initializable {


    @FXML
    private JFXTextField tfSearch;

    @FXML
    private Button btnCreateAccount;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btnCreateAccount.setOnAction(event -> {
            AppData.newUserAddRole = AppData.UserRole.MATERIAL.name();
            new AppFunction().showDialog("/users/main_office/sub_fxml/add_new_account.fxml");
        });


    }
}
